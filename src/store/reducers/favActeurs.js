const initialState = { favActeursID: [] }

function favActeurs(state = initialState, action) {
  let nextState
  switch (action.type) {
    case 'SAVE_ACTEUR':
      nextState = {
        ...state,
        favActeursID: [...state.favActeursID, action.value]
      };
      return nextState || state
    case 'UNSAVE_ACTEUR':
      nextState = {
        ...state,
        favActeursID: state.favActeursID.filter(id => id !== action.value)
      };
      return nextState || state
    default:
      return state
  };
}

export default favActeurs;