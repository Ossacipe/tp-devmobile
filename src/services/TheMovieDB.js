const API_KEY = 'f60dc1588d1b92e483f83fa137b9f5ab';

export async function getActeurs(searchTerm = '', offset = 1) {
  try {
    let url;
    if(searchTerm==''){
      url = `https://api.themoviedb.org/3/person/popular?api_key=${API_KEY}&language=en-US&page=${offset}`
    }
    else {
      url = `https://api.themoviedb.org/3/search/person?api_key=${API_KEY}&language=en-US&query=${searchTerm}&page=${offset}&include_adult=false`
    }
    const response = await fetch(url);
    const json = await response.json();
    return json;
  } catch (error) {
    console.log(`Error with function getRestaurantDetails ${error.message}`);
    throw error;
  }
};

export async function getActeurDetails(acteurId) {
  try {
    const url = `https://api.themoviedb.org/3/person/${acteurId}?api_key=${API_KEY}&language=en-US`;
    const response = await fetch(url);
    const json = await response.json();
    return json;
  } catch (error) {
    console.log(`Error with function getRestaurantDetails ${error.message}`);
    throw error;
  }
};

export async function getActeurCombinedCredits(acteurId) {
  try {
    const url = `https://api.themoviedb.org/3/person/${acteurId}/combined_credits?api_key=${API_KEY}&language=en-US`;
    const response = await fetch(url);
    const json = await response.json();
    return json;
  } catch (error) {
    console.log(`Error with function getRestaurantDetails ${error.message}`);
    throw error;
  }
};