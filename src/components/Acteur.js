import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, ActivityIndicator, ScrollView, Image, Button } from 'react-native';
import { connect } from 'react-redux';
import Toast from 'react-native-root-toast';

import DisplayError from '../components/DisplayError';

import { getActeurDetails,getActeurCombinedCredits } from '../services/TheMovieDB';

import Colors from '../definitions/Colors';

const Acteur = ({ route, favActeurs, dispatch }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [acteur, setActeur] = useState(null);
  const [acteurCombinedCredits, setActeurCombinedCredits] = useState(null);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    requestActeur();
  }, []); // Uniquement à l'initialisation

  // Pourrait être directement déclarée dans useEffect
  const requestActeur = async () => {
    try {
      const themoviedbActeurResult = await getActeurDetails(route.params.acteurID);
      setActeur(themoviedbActeurResult);
      const themoviedbActeurCombinedCreditsResult = await getActeurCombinedCredits(route.params.acteurID);
      setActeurCombinedCredits(themoviedbActeurCombinedCreditsResult);
      setIsLoading(false);
    } catch (error) {
      setIsError(true);
    }
  }

  // On pourrait définir les actions dans un fichier à part

  const saveActeur = async () => {
    const action = { type: 'SAVE_ACTEUR', value: route.params.acteurID };
    dispatch(action);
    let toast = Toast.show('Acteur ajouté aux favoris', {
      duration: Toast.durations.LONG,
    });
  }

  const unsaveActeur = async () => {
    const action = { type: 'UNSAVE_ACTEUR', value: route.params.acteurID };
    dispatch(action);
    let toast = Toast.show('Acteur retiré des favoris', {
      duration: Toast.durations.LONG,
    });
  }

  const displaySaveActeur = () => {
    if (favActeurs.findIndex(i => i === route.params.acteurID) !== -1) {
      // L'acteur est sauvegardé
      return (
        <Button
          title='Retirer des favoris'
          color={Colors.mainBlue}
          onPress={unsaveActeur}
        />
      );
    }
    // L'acteur n'est pas sauvegardé
    return (
      <Button
        title='Ajouter aux favoris'
        color={Colors.mainBlue}
        onPress={saveActeur}
      />
    );
  }

  const displayActeurCombinedCredits = () => {
    let lstMedias = "";
    acteurCombinedCredits.cast.forEach(element => lstMedias += element.original_title + ", ");
    return lstMedias;
  }

  return (
    <View style={styles.container}>
      {isError ?
        (<DisplayError message='Impossible de récupérer les données de lacteur' />) :
        (isLoading ?
          (<View style={styles.containerLoading}>
            <ActivityIndicator size="large" />
          </View>) :

          (<ScrollView style={styles.containerScroll}>
            <View style={styles.containerCardTop}>
              <View style={styles.containerEstab}>
                <Text style={styles.textName}>
                  {acteur.name}
                </Text>
              </View>
              <View style={[styles.containerButtonFav]}>
                {displaySaveActeur()}
              </View>
            </View>
            <View style={styles.containerCardBottom}>
              <Text style={[styles.textTitle, { marginTop: 0 }]}>
                Birth
              </Text>
              <Text style={styles.textContent}>
                {acteur.birthday} ({acteur.place_of_birth})
              </Text>
              <Text style={styles.textTitle}>
                Death
              </Text>
              <Text style={styles.textContent}>
                {acteur.deathday}
              </Text>
              <Text style={styles.textTitle}>
                Play in
              </Text>
              <Text style={styles.textContent}>
               {displayActeurCombinedCredits()}
              </Text>
              <Text style={styles.textTitle}>
                Bio
              </Text>
              <Text>{acteur.biography}</Text>
            </View>
          </ScrollView>)
        )}
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    favActeurs: state.favActeursID
  }
}

export default connect(mapStateToProps)(Acteur);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerScroll: {
    flex: 1,
    paddingHorizontal: 12,
    paddingVertical: 16,
  },
  containerCardTop: {
    elevation: 1,
    borderRadius: 3,
    padding: 12,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  containerCardBottom: {
    elevation: 1,
    marginTop: 16,
    borderRadius: 3,
    padding: 12,
    backgroundColor: 'white',
  },
  containerNoActeurImage: {
    height: 128,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    backgroundColor: 'white',
  },
  acteurImage: {
    height: 180,
    backgroundColor: Colors.mainBlue,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },
  containerEstab: {
    flex: 2,
  },
  containerButtonFav: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 3,
    flexDirection: 'row',
    alignItems: 'flex-end',
    flex: 1,
    justifyContent: 'center',
  },
  containerNote: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 3,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  textNote: {
    color: 'white',
    fontWeight: "bold",
    fontSize: 16,
  },
  textMaxNote: {
    fontSize: 12,
    marginLeft: 3,
    color: 'white',
  },
  textVotes: {
    fontStyle: "italic",
    fontSize: 12,
  },
  textName: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  textTitle: {
    fontWeight: 'bold',
    color: Colors.mainBlue,
    fontSize: 16,
    marginTop: 16,
  },
  textContent: {
    fontSize: 16,
  },
});