import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import Colors from '../definitions/Colors';

const ActeurListItem = ({ onClick, acteurData }) => {

  const getThumbnail = () => {
    if (acteurData.profile_path) {
      return (
        <Image style={styles.thumbnail} source={{ uri: 'https://image.tmdb.org/t/p/w500' + acteurData.profile_path }} />
      );
    };
    return (
      <View style={styles.noThumbnailContainer}>
      </View>
    );
  };

  return (
    <TouchableOpacity style={styles.container}
      onPress={() => { onClick(acteurData.id) }}>
      {getThumbnail()}
      <View style={styles.informationContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>
            {acteurData.name}
          </Text>
          <Text>
            {acteurData.popularity}
          </Text>
        </View>
        <Text style={[styles.data, styles.cuisine]}
          numberOfLines={1}>
          {acteurData.known_for_department}
        </Text>
        <View style={styles.statsContainer}>
          <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ActeurListItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 8,
  },
  informationContainer: {
    flex: 1,
    marginLeft: 12,
    justifyContent: 'center',
  },
  titleContainer: {
    flexDirection: 'row',
  },
  statsContainer: {
    flexDirection: 'row',
    marginTop: 12,
  },
  statContainer: {
    flexDirection: 'row',
    marginRight: 8,
  },
  noThumbnailContainer: {
    width: 128,
    height: 128,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.mainBlue,
    borderRadius: 10
  },
  thumbnail: {
    width: 128,
    height: 128,
    borderRadius: 12,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  data: {
    fontSize: 16,
  },
  cuisine: {
    fontStyle: 'italic',
  },
  icon: {
    tintColor: Colors.mainBlue,
  },
  stat: {
    marginLeft: 4,
  },
});