import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';

import ActeurListItem from '../components/ActeurListItem';
import DisplayError from '../components/DisplayError';

import { getActeurDetails } from '../services/TheMovieDB';

const Favoris = ({ navigation, favActeurs }) => {

  const [acteurs, setActeurs] = useState([]);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    refreshFavActeurs();
  }, [favActeurs]); // A chaque fois que les acteurs favoris changent

  const refreshFavActeurs = async () => {
    setIsRefreshing(true);
    setIsError(false);
    let acteurs = [];
    try {
      for (const id of favActeurs) {
        const themoviedbSearchResult = await getActeurDetails(id)
        acteurs.push(themoviedbSearchResult);
      };
      setActeurs(acteurs);
    } catch (error) {
      setIsError(true);
      setActeurs([]);
    }
    setIsRefreshing(false);
  };

  const navigateToActeurDetails = (acteurID) => {
    navigation.navigate("ViewActeur", { acteurID });
  };

  const amIaFavActeur = (acteurID) => {
    if (favActeurs.findIndex(i => i === acteurID) !== -1) {
      return true;
    }
    return false;
  };

  return (
    <View style={styles.container}>
      {
        isError ?
          (<DisplayError message='Impossible de récupérer les acteurs favoris' />) :
          (<FlatList
            data={acteurs}
            extraData={favActeurs}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <ActeurListItem
                acteurData={item}
                onClick={navigateToActeurDetails}
                isFav={amIaFavActeur(item.id)} />
            )}
            refreshing={isRefreshing}
            onRefresh={refreshFavActeurs}
          />)
      }
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    favActeurs: state.favActeursID
  }
}

export default connect(mapStateToProps)(Favoris);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 12,
    marginTop: 16,
  },
});