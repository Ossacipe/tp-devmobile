import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import Colors from '../definitions/Colors';

const DisplayError = ({ message = "Une erreur c'est produite" }) => (
  <View style={styles.container}>
    <Text style={styles.errorText}>
      {message}
    </Text>
  </View>
);

export default DisplayError;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    tintColor: Colors.mainBlue,
  },
  errorText: {
    fontSize: 16,
  },
});