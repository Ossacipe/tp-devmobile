import React, { useState, useEffect } from 'react';
import { View, TextInput, Button, StyleSheet, FlatList, Keyboard } from 'react-native';
import { connect } from 'react-redux';

import ActeurlistItem from '../components/ActeurListItem';
import DisplayError from '../components/DisplayError';

import Colors from '../definitions/Colors';

import { getActeurs } from '../services/TheMovieDB';

const Search = ({ navigation, favActeurs }) => {

  const [acteurs, setActeurs] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [nextOffset, setNextOffset] = useState(1);
  const [isMoreResults, setIsMoreResults] = useState(true);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    requestActeurs([], 1);
  }, []); // Uniquement à l'initialisation

  const requestActeurs = async (prevActeurs, offset) => {
    setIsRefreshing(true);
    setIsError(false);
    try {
      const themoviedbSearchResult = await getActeurs(searchTerm, offset);
      setActeurs([...prevActeurs, ...themoviedbSearchResult.results]);
      if (themoviedbSearchResult.page < themoviedbSearchResult.total_pages) {
        setIsMoreResults(true);
        setNextOffset(themoviedbSearchResult.page + 1);
      } else {
        setIsMoreResults(false);
      }
    } catch (error) {
      setIsError(true);
      setActeurs([]);
      setIsMoreResults(true);
      setNextOffset(1);
    }
    setIsRefreshing(false);
  };

  const searchActeurs = () => {
    Keyboard.dismiss();
    requestActeurs([], 1);
  };

  const annuler = () => {
    setSearchTerm('');
    requestActeurs([], 1);
  };

  const loadMoreActeurs = () => {
    if (isMoreResults) {
      requestActeurs(acteurs, nextOffset);
    };
  };

  const navigateToActeurDetails = (acteurID) => {
    navigation.navigate("ViewActeur", { acteurID });
  };

  const amIaFavActeur = (acteurID) => {
    if (favActeurs.findIndex(i => i === acteurID) !== -1) {
      return true;
    }
    return false;
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <TextInput
          placeholder='Nom de lacteur'
          style={styles.inputActeurName}
          onChangeText={(text) => setSearchTerm(text)}
          onSubmitEditing={searchActeurs}
        />
        <Button
          title='Rechercher'
          color={Colors.mainBlue}
          onPress={searchActeurs}
        />
        <Button
          title='Annuler'
          color={Colors.mainBlue}
          onPress={annuler}
        />
      </View>
      {
        isError ?
          (<DisplayError message='Impossible de récupérer les acteurs' />) :
          (<FlatList
            data={acteurs}
            extraData={favActeurs}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <ActeurlistItem
                acteurData={item}
                onClick={navigateToActeurDetails}
                isFav={amIaFavActeur(item.id)} />
            )}
            onEndReached={loadMoreActeurs}
            onEndReachedThreshold={0.5}
            refreshing={isRefreshing}
            onRefresh={searchActeurs}
          />)
      }
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    favActeurs: state.favActeursID
  }
}

export default connect(mapStateToProps)(Search);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 12,
    marginTop: 16,
  },
  searchContainer: {
    marginBottom: 16,
  },
  inputActeurName: {
    marginBottom: 8,
  },
});