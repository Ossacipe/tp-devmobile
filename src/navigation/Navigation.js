import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Image } from 'react-native';

import Search from '../components/Search';
import Acteur from '../components/Acteur';
import Favoris from '../components/Favoris';

import Colors from '../definitions/Colors';

const SearchNavigation = createStackNavigator();
const FavNavigation = createStackNavigator();
const TabNavigation = createBottomTabNavigator();

function searchStackScreens() {
  return (
    <SearchNavigation.Navigator
      initialRouteName="ViewSearch"
    >
      <SearchNavigation.Screen
        name="ViewSearch"
        component={Search}
        options={{ title: 'Recherche' }}
      />
      <SearchNavigation.Screen
        name="ViewActeur"
        component={Acteur}
        options={{ title: 'Acteur' }}
      />
    </SearchNavigation.Navigator>
  )
};

function favStackScreens() {
  return (
    <FavNavigation.Navigator
      initialRouteName="ViewFav"
    >
      <FavNavigation.Screen
        name="ViewFav"
        component={Favoris}
        options={{ title: 'Favoris' }}
      />
      <FavNavigation.Screen
        name="ViewActeur"
        component={Acteur}
        options={{ title: 'Acteur' }}
      />
    </FavNavigation.Navigator>
  )
};

function RootStack() {
  return (
    <TabNavigation.Navigator
      screenOptions={{
        tabBarActiveTintColor: Colors.mainBlue,
        headerShown: false
      }}>
      <TabNavigation.Screen
        name="Recherche"
        component={searchStackScreens}
      />
      <TabNavigation.Screen
        name="Favoris"
        component={favStackScreens}
      />
    </TabNavigation.Navigator>
  );
}

export default RootStack;