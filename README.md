## Fonctionnalités réalisées

* Récupérer la liste des personnes en tendance : OK
* Charger plus de résultats : OK
* Récupérer la liste des personnes avec une recherche : OK
* Annuler une recherche : Marche partiellement : n'enlève pas la valeur saisie dans l'input et nécessite une actualisation
* Naviguer vers la page de détails (depuis la recherche) : OK
* Afficher les informations de base : OK
* Afficher la liste des médias : OK (Parfois des undefined dans la liste)
* Mettre / enlever des favoris : OK
* Avoir 2 tabs (recherche et favoris) : OK
* Affichage des favoris : OK
* Naviguer vers la page de détails (depuis les favoris) : OK



## Remarques éventuelles

Merci d'indiquer si vous avez fait l'exercice sur téléphone ou émulateur. 

=> Execrice sur Téléphone via l'appli Expo Go

Ajouter vos remarques ici si besoin